typedef struct {
  int lex_code;
  char *text;
  int lineno;
  int column;
} scanner_state_t;
