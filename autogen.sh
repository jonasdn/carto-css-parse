#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

PKG_NAME="carto-css-parse"


which gnome-autogen.sh || {
    echo "You need to install gnome-common."
    exit 1
}

REQUIRED_AUTOCONF_VERSION=2.59
REQUIRED_AUTOMAKE_VERSION=1.9
. gnome-autogen.sh
