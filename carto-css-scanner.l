%{
  #include "carto-css-parser.h"
  #define YY_USER_ACTION \
  do { \
    yyset_column(yyget_column(yyscanner) + yyget_leng(yyscanner), yyscanner); \
  } while (0);

  int line_number = 1;
%}

%option noyywrap
%option reentrant
%option yylineno

ident          #?[a-z][a-zA-Z-]*
num            [0-9]+|[0-9]*"."[0-9]+
var            @[a-z][a-zA-Z-]*
hexcolor       #([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})
%%

{ident}         { return IDENT; }
{num}           { return NUM; }
{var}           { return VAR; }
{hexcolor}      { return HEXCOLOR; }
"{"             { return LBRACE; }
"}"             { return RBRACE; }
";"             { return SEMICOLON; }
":"             { return COLON; }
[\n\r]          { yyset_lineno(++line_number, yyscanner); }
.|\n            { }
%%
