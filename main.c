#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include "carto-css-parser.h"
#include "carto-css-scanner.h"

#include "carto-css-parser.c"

void parse(char *line, size_t size) {
  yyscan_t scanner;
  YY_BUFFER_STATE buffer_state;
  void *carto_css_parser;
  scanner_state_t scanner_state;

  yylex_init(&scanner);

  buffer_state = yy_scan_buffer(line, size, scanner);
  carto_css_parser = ParseAlloc(malloc);

  do {
    scanner_state.lex_code = yylex(scanner);
    scanner_state.text = yyget_text(scanner);
    scanner_state.lineno = yyget_lineno(scanner);
    scanner_state.column = yyget_column(scanner);
    printf("%-15s %s\n",
           scanner_state.text,
           yyTokenName[scanner_state.lex_code]);
    Parse(carto_css_parser, scanner_state.lex_code, NULL, scanner_state);
  } while (scanner_state.lex_code > 0);

  printf("Parsed succesfully!\n");

  yy_delete_buffer(buffer_state, scanner);
  yylex_destroy(scanner);
  ParseFree(carto_css_parser, free);
}

int main (int argc, char **argv) {

  FILE * fp;
  char * buf = NULL;
  size_t len = 0;
  ssize_t read;

  if (argc != 2)
    exit(EXIT_FAILURE);

  fp = fopen(argv[1], "r");
  if (fp == NULL)
    exit(EXIT_FAILURE);

  fseek(fp, 0, SEEK_END);
  len = ftell(fp);
  fseek(fp, 0, SEEK_SET);

  buf = malloc(len + 2);
  fread(buf, len, 1, fp);
  fclose(fp);

  buf[len] = 0;
  buf[len + 1] = 0;
  parse(buf, len + 2);

  if (buf)
    free(buf);

  exit(EXIT_SUCCESS);
}
