%include {
  #include <stdio.h>
  #include <assert.h>

  #include "typedefs.h"
  #include "carto-css-scanner.h"
}

%extra_argument { scanner_state_t scanner_state }
%syntax_error {
  int i;
  int n;

  printf("Unexpected token '%s' at line %d:%d, valid tokens:",
         scanner_state.text,
         scanner_state.lineno,
         scanner_state.column);

  n = sizeof(yyTokenName) / sizeof(yyTokenName[0]);
  for (i = 0; i < n; ++i) {
    int a;

    a = yy_find_shift_action(yypParser, (YYCODETYPE)i);
    if (a < YYNSTATE + YYNRULE) {
      printf(" %s", yyTokenName[i]);
    }
  }

  printf("\n");
  exit(1);
}

stylesheet ::= rule_list .
rule_list ::= rule_list rule .
rule_list ::= .
rule ::= selector LBRACE declaration_list RBRACE .
rule ::= VAR COLON value SEMICOLON .
selector ::= IDENT .
declaration_list ::= declaration_list declaration .
declaration_list ::= .
declaration ::= property COLON value SEMICOLON .
property ::= IDENT .
value ::= IDENT .
value ::= NUM .
value ::= VAR .
value ::= HEXCOLOR .
